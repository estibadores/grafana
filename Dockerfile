FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get install -y --no-install-recommends ca-certificates && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /etc/apt/keyrings/
COPY gpg.key /etc/apt/keyrings/grafana.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | tee -a /etc/apt/sources.list.d/grafana.list && \
    apt-get update && \
    apt-get install -y --no-install-recommends grafana && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

## Install Plugin
RUN grafana-cli plugins install grafana-piechart-panel

WORKDIR "/usr/share/grafana"
VOLUME ["/usr/share/grafana/data"]
EXPOSE 3000

ENTRYPOINT ["/usr/sbin/grafana-server"]
CMD ["web"]
